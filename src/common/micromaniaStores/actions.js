// @flow
import type { Action, Store } from '../types';
import {rawStores} from './stores_datas';

export const loadDatas = (center) : Action => {
  return querydb(rawStores());
};

function querydb(stores){
    return {
      type: 'QUERY_STORE_DB',
      payload: {stores},
    };
}

export const selectStore = (id) : Action => {
  return {
    type: 'ADD_STORE_TO_SELECTION',
    payload: {id},
  };
};

export const unselectStore = (id) : Action => {
  return {
    type: 'REMOVE_STORE_TO_SELECTION',
    payload: {id},
  };
};