// @flow
import type { Action, StoresStates } from '../types';
import { append } from 'ramda';

const initialState = [];

const reducer = (
  state: StoresState = initialState,
  action: Action,
): StoresState => {
  switch (action.type) {
    case 'QUERY_STORE_DB': {
      const dbStores = action.payload.stores;
      console.log(action.payload.stores);
      const stores = dbStores.map(store => {
        return store
      })
      return stores
    }
    case 'ADD_STORE_TO_SELECTION': {
      const { id } = action.payload;
      const updatedStores = state.map(store => {
        if (store.id === id){
          return {...store, selected:true}
        }
        return store
      })
      return updatedStores
    }
    case 'REMOVE_STORE_TO_SELECTION': {
      const { id } = action.payload;
      const updatedStores = state.map(store => {
        if (store.id === id){
          return {...store, selected:false}
        }
        return store
      })
      return updatedStores
    }
    default:
      return state;
  }
};

export default reducer;
