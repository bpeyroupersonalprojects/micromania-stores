
  export const rawStores = () => {
    return([
    {
        "address": "1  avenue Niel 75017 paris 17e",
        "lat": 48.87893454,
        "id": "6878",
        "lng": 2.29504493,
        "name": "Micromania 6878"
    },
    {
        "address": "Centre commercial BEAUGRENELLE Rue de Linois 75015 paris 15e",
        "lat": 48.84833687,
        "id": "6662",
        "lng": 2.28397166,
        "name": "Micromania 6662"
    },
    {
        "address": "Hall RER La Grande Arche Lot 103506 92800 puteaux",
        "lat": 48.89177159,
        "id": "6757",
        "lng": 2.2393473,
        "name": "Micromania 6757"
    },
    {
        "address": "Centre commercial 4 TEMPS 2 Le Parvis de la D\u00e9fense 92800 puteaux",
        "lat": 48.89141725,
        "id": "6701",
        "lng": 2.23478235,
        "name": "Micromania 6701"
    },
    {
        "address": "Rue Caumartin 75009 paris 9e",
        "lat": 48.87559845,
        "id": "6694",
        "lng": 2.32923157,
        "name": "Micromania 6694"
    },
    {
        "address": "365 Rue de Vaugirard 75015 paris 15e",
        "lat": 48.8364968,
        "id": "167011",
        "lng": 2.2952426,
        "name": "Micromania 167011"
    },
    {
        "address": "5  Bld des Italiens Passages des Princes 75002 paris 2e",
        "lat": 48.87144653,
        "id": "6871",
        "lng": 2.33973588,
        "name": "Micromania 6871"
    },
    {
        "address": "Gare Paris Montparnasse Hall Vasarely - 17 rue de Vaugirard 75015 paris 15e",
        "lat": 48.84106753,
        "id": "6732",
        "lng": 2.31948823,
        "name": "Micromania 6732"
    },
    {
        "address": "126  Rue de Rennes 75006 paris 6e",
        "lat": 48.84664073,
        "id": "6767",
        "lng": 2.32723435,
        "name": "Micromania 6767"
    },
    {
        "address": "210 Porte Lescot 75001 paris 1er",
        "lat": 48.86170366,
        "id": "6740",
        "lng": 2.34861621,
        "name": "Micromania 6740"
    },
    {
        "address": "Centre commercial CARREFOUR 21-23 RUE LOUIS CALMEL 92230 gennevilliers",
        "lat": 48.92465952,
        "id": "6728",
        "lng": 2.29058917,
        "name": "Micromania 6728"
    },
    {
        "address": "Gare du Nord Mezzanine Banlieu local 9 18 Rue de Dunkerque 75010 paris 10e",
        "lat": 48.8819243,
        "id": "6733",
        "lng": 2.35607418,
        "name": "Micromania 6733"
    },
    {
        "address": "Centre commercial ITALIE 2 30  Avenue D\u00b4Italie - boite 1031 75013 paris 13e",
        "lat": 48.82985964,
        "id": "6754",
        "lng": 2.35687202,
        "name": "Micromania 6754"
    },
    {
        "address": "Centre commercial La Vache Noire 1 place de la Vache Noire 94110 arcueil",
        "lat": 48.8097822,
        "id": "6626",
        "lng": 2.32724145,
        "name": "Micromania 6626"
    },
    {
        "address": "Centre commercial OKABE Avenue de Fontainebleau 94270 le kremlin bicetre",
        "lat": 48.81105615,
        "id": "6592",
        "lng": 2.3627706,
        "name": "Micromania 6592"
    },
    {
        "address": "Centre commercial Carrefour du Plateau Avenue Robert SCHUMAN 78500 sartrouville",
        "lat": 48.94651469,
        "id": "6865",
        "lng": 2.19735918,
        "name": "Micromania 6865"
    },
    {
        "address": "Centre commercial CARREFOUR 3  Rue de Horrione 95110 sannois",
        "lat": 48.96448856,
        "id": "6866",
        "lng": 2.25592849,
        "name": "Micromania 6866"
    },
    {
        "address": "Centre commercial BERCY2 Place de l\u00b4Europe - Local 227 94200 ivry sur seine",
        "lat": 48.82522757,
        "id": "6649",
        "lng": 2.39498191,
        "name": "Micromania 6649"
    },
    {
        "address": "Centre commercial BEL EST 26 av du G\u00e9n\u00e9ral de Gaulle 93170 bagnolet",
        "lat": 48.86544368,
        "id": "6651",
        "lng": 2.41631766,
        "name": "Micromania 6651"
    },
    {
        "address": "Centre commercial IVRY GRAND CIEL Local N\u00b053 94200 ivry sur seine",
        "lat": 48.81947433,
        "id": "6726",
        "lng": 2.39718029,
        "name": "Micromania 6726"
    },
    {
        "address": "Centre commercial VELIZY 2 78140 velizy villacoublay",
        "lat": 48.77968063,
        "id": "6894",
        "lng": 2.2225185,
        "name": "Micromania 6894"
    },
    {
        "address": "Centre commercial CARREFOUR 280 av Gabriel P\u00e9ri 78360 montesson",
        "lat": 48.92891068,
        "id": "6835",
        "lng": 2.14643937,
        "name": "Micromania 6835"
    },
    {
        "address": "Centre commercial Carrefour Avenir 60  Rue Saint Stenay 93700 drancy",
        "lat": 48.9186618,
        "id": "6915",
        "lng": 2.42249414,
        "name": "Micromania 6915"
    },
    {
        "address": "Centre commercial CARREFOUR 66/67 Avenue de Stalingrad 94800 villejuif",
        "lat": 48.78227544,
        "id": "6899",
        "lng": 2.36856367,
        "name": "Micromania 6899"
    },
    {
        "address": "Centre commercial R\u00e9gional de PARLY II Niveau bas 141 local postal 341 78150 le chesnay",
        "lat": 48.82799507,
        "id": "6820",
        "lng": 2.11713607,
        "name": "Micromania 6820"
    },
    {
        "address": "Centre commercial PARLY 2 Local 128 78150 le chesnay",
        "lat": 48.82811156,
        "id": "6808",
        "lng": 2.11694501,
        "name": "Micromania 6808"
    },
    {
        "address": "27  Avenue de la Division Leclerc 92160 antony",
        "lat": 48.75185052,
        "id": "6827",
        "lng": 2.30553243,
        "name": "Micromania 6827"
    },
    {
        "address": "Centre commercial du Pav\u00e9 de Montigny 95370 montigny les cormeilles",
        "lat": 48.99396658,
        "id": "6617",
        "lng": 2.19940213,
        "name": "Micromania 6617"
    },
    {
        "address": "Centre commercial BELLE EPINE Local 23 - Belle Epine 440 94320 thiais",
        "lat": 48.75630774,
        "id": "6712",
        "lng": 2.37227256,
        "name": "Micromania 6712"
    },
    {
        "address": "Centre commercial ROSNY 2 93110 rosny sous bois",
        "lat": 48.88297289,
        "id": "6837",
        "lng": 2.47981652,
        "name": "Micromania 6837"
    },
    {
        "address": "Gare Saint Charles  SAINT-CHARLES 13001 marseille 1er",
        "lat": 43.302312403185,
        "id": "6921",
        "lng": 5.3802012161161,
        "name": "Micromania 6921"
    },
    {
        "address": "Centre commercial CARREFOUR LE MERLAN Avenue Raimu 13014 marseille 14e",
        "lat": 43.33017321,
        "id": "6765",
        "lng": 5.40041261,
        "name": "Micromania 6765"
    },
    {
        "address": "Centre commercial AUCHAN 57 BD ROMAIN ROLLAND 13010 marseille 10e",
        "lat": 43.27893316,
        "id": "6771",
        "lng": 5.42554206,
        "name": "Micromania 6771"
    },
    {
        "address": "Centre commercial BONNEVEINE 112 Av de Hambourg 13008 marseille 8e",
        "lat": 43.24646417,
        "id": "6668",
        "lng": 5.39461958,
        "name": "Micromania 6668"
    },
    {
        "address": "Centre commercial GRAND LITTORAL 11  Avenue Saint Antoine 13015 marseille 15e",
        "lat": 43.36625575,
        "id": "6731",
        "lng": 5.35735148,
        "name": "Micromania 6731"
    },
    {
        "address": "Centre commercial LE PRIMPTEMPS TRAVERSE MONTRE 13011 marseille 11e",
        "lat": 43.29521368,
        "id": "6609",
        "lng": 5.47635199,
        "name": "Micromania 6609"
    },
    {
        "address": "Centre commercial GEANT LA VALENTINE Route de la Sabli\u00e8re 13011 marseille 11e",
        "lat": 43.2926681,
        "id": "6774",
        "lng": 5.48006599,
        "name": "Micromania 6774"
    },
    {
        "address": "Centre commercial GEANT CASINO 13170 les pennes mirabeau",
        "lat": 43.41621582,
        "id": "6811",
        "lng": 5.36173794,
        "name": "Micromania 6811"
    },
    {
        "address": "CCial avant cap local 51 13480 cabries",
        "lat": 43.42106416,
        "id": "6623",
        "lng": 5.36650956,
        "name": "Micromania 6623"
    },
    {
        "address": "Centre commercial GRAND VITROLLES Quartier du Griffon - RN 113 13127 vitrolles",
        "lat": 43.43306237,
        "id": "6907",
        "lng": 5.25929394,
        "name": "Micromania 6907"
    },
    {
        "address": "Centre commercial AUCHAN Les Paluds 13400 aubagne",
        "lat": 43.28949432,
        "id": "6641",
        "lng": 5.59842908,
        "name": "Micromania 6641"
    },
    {
        "address": "Centre commercial Carrefour 13220 chateauneuf les martigues",
        "lat": 43.38296,
        "id": "166996",
        "lng": 5.164636,
        "name": "Micromania 166996"
    },
    {
        "address": "Centre commercial CARREFOUR LES MILLES 1175 rue G du Vair 13290 aix en provence",
        "lat": 43.50579012,
        "id": "6644",
        "lng": 5.39631534,
        "name": "Micromania 6644"
    },
    {
        "address": "Centre commercial GEANT ROUTE DE BERRE 13100 aix en provence",
        "lat": 43.53565569,
        "id": "6636",
        "lng": 5.40503464,
        "name": "Micromania 6636"
    },
    {
        "address": "Les all\u00e9es provencales - All\u00e9e Guiseppe Verdi 13100 aix en provence",
        "lat": 43.5338335,
        "id": "166995",
        "lng": 5.509247,
        "name": "Micromania 166995"
    },
    {
        "address": "CCial Auchan Av Paul Eluard 13500 martigues",
        "lat": 43.42187185,
        "id": "6760",
        "lng": 5.05419525,
        "name": "Micromania 6760"
    },
    {
        "address": "55 chemin Bouy\u00e8re 83190 ollioules",
        "lat": 43.12040301,
        "id": "6798",
        "lng": 5.87166298,
        "name": "Micromania 6798"
    },
    {
        "address": "Centre commercial AUCHAN Quartier Lery RN 63 83500 la seyne sur mer",
        "lat": 43.09888335,
        "id": "6608",
        "lng": 5.90310787,
        "name": "Micromania 6608"
    },
    {
        "address": "Centre commercial MAYOL Rue des Muriers 83000 toulon",
        "lat": 43.11945272,
        "id": "6615",
        "lng": 5.93698931,
        "name": "Micromania 6615"
    },
    {
        "address": "Centre commercial GRAND VAR Local 22 83160 la valette du var",
        "lat": 43.13667781,
        "id": "6738",
        "lng": 6.00515652,
        "name": "Micromania 6738"
    },
    {
        "address": "Centre Commercial Leclerc RN 7 QUARTIER SAINT JEAN 83170 brignoles",
        "lat": 43.40667442,
        "id": "6734",
        "lng": 6.04879578,
        "name": "Micromania 6734"
    },
    {
        "address": "Centre commercial G\u00e9ant Zac du Roubaud - Chemin du rocher 83400 hyeres",
        "lat": 43.11543834,
        "id": "6750",
        "lng": 6.11048776,
        "name": "Micromania 6750"
    },
    {
        "address": "Centre commercial GEANT ARLES Zone am\u00e9nagement Fourchon 13200 arles",
        "lat": 43.66703755,
        "id": "6632",
        "lng": 4.63970476,
        "name": "Micromania 6632"
    },
    {
        "address": "Centre commercial AUCHAN MISTRAL 7 84000 avignon",
        "lat": 43.92209567,
        "id": "6633",
        "lng": 4.86431509,
        "name": "Micromania 6633"
    },
    {
        "address": "Centre commercial Carrefour Route de Nimes 30300 beaucaire",
        "lat": 43.81709247,
        "id": "6797",
        "lng": 4.61798232,
        "name": "Micromania 6797"
    },
    {
        "address": "Centre commercial AUCHAN PONTET Espace Soleil Avignon Nord N\u00b0119 84130 le pontet",
        "lat": 43.98336851,
        "id": "6642",
        "lng": 4.88249112,
        "name": "Micromania 6642"
    },
    {
        "address": "RN 555 1303 Route de Draguignan 83720 trans en provence",
        "lat": 43.50020717,
        "id": "6870",
        "lng": 6.48622888,
        "name": "Micromania 6870"
    },
    {
        "address": "Centre commercial NIMES SUD Avenue Pierre Mendes France 30000 nimes",
        "lat": 43.82037657,
        "id": "6791",
        "lng": 4.38714537,
        "name": "Micromania 6791"
    },
    {
        "address": "Centre commercial GEANT Lot 19 Lieu dit Le Mas de Vignobles 30000 nimes",
        "lat": 43.81040635,
        "id": "6784",
        "lng": 4.36114524,
        "name": "Micromania 6784"
    },
    {
        "address": "Centre commercial La Coupole des Halles 22  Boulevard Gambetta 30000 nimes",
        "lat": 43.84047123,
        "id": "6785",
        "lng": 4.35905946,
        "name": "Micromania 6785"
    },
    {
        "address": "Centre commercial Meriadeck 33000 bordeaux",
        "lat": 44.83865037,
        "id": "6659",
        "lng": -0.58345132,
        "name": "Micromania 6659"
    },
    {
        "address": "Centre commercial BORDEAUX LAC 33000 bordeaux",
        "lat": 44.880284910569,
        "id": "6658",
        "lng": -0.567666162684,
        "name": "Micromania 6658"
    },
    {
        "address": "Centre commercial AUCHAN BORDEAUX BOULIAC CD 113 lieu dit Bonneau 33270 bouliac",
        "lat": 44.8151142,
        "id": "6667",
        "lng": -0.52293305,
        "name": "Micromania 6667"
    },
    {
        "address": "Centre Commercial CARREFOUR MERIGNAC SOLEIL ROUTE DU CAP FERRET 33700 merignac",
        "lat": 44.82911642,
        "id": "6759",
        "lng": -0.65710764,
        "name": "Micromania 6759"
    },
    {
        "address": "Centre commercial CARREFOUR BEGLES Les Rives D\u00b4Arcins 33130 begles",
        "lat": 44.79478347,
        "id": "6648",
        "lng": -0.5305138,
        "name": "Micromania 6648"
    },
    {
        "address": "Centre commercial Carrefour Les quatres Pavillons 33310 lormont",
        "lat": 44.8668586,
        "id": "6599",
        "lng": -0.51596346,
        "name": "Micromania 6599"
    },
    {
        "address": "Centre commercial GRAND TOUR Avenue de l\u00b4Aquitaine 33560 sainte eulalie",
        "lat": 44.90816688,
        "id": "6855",
        "lng": -0.48565189,
        "name": "Micromania 6855"
    },
    {
        "address": "Centre commercial AUCHAN 31 Rue des Fonderies lieu dit Facture 33380 biganos",
        "lat": 44.63797086,
        "id": "6655",
        "lng": -0.9610571,
        "name": "Micromania 6655"
    },
    {
        "address": "CENTRE COMMERCIAL CAP OCEAN 33260 la teste de buch",
        "lat": 44.63651242,
        "id": "6795",
        "lng": -1.15275337,
        "name": "Micromania 6795"
    },
    {
        "address": "Centre commercial LECLERC Rives de Dordogne 24100 bergerac",
        "lat": 44.83418126,
        "id": "6724",
        "lng": 0.45064739,
        "name": "Micromania 6724"
    },
    {
        "address": "Centre commercial AUCHAN COGNAC ROCADE DE COGNAC 16100 chateaubernard",
        "lat": 45.68370936,
        "id": "6687",
        "lng": -0.30588702,
        "name": "Micromania 6687"
    },
    {
        "address": "ROUTE DE BORDEAUX 16400 la couronne",
        "lat": 45.6173944,
        "id": "6631",
        "lng": 0.10464939,
        "name": "Micromania 6631"
    },
    {
        "address": "Centre commercial Auchan Avenue Louis Suder 24430 marsac sur l isle",
        "lat": 45.19282295,
        "id": "6614",
        "lng": 0.65876414,
        "name": "Micromania 6614"
    },
    {
        "address": "Centre commercial LECLERC La Feuilleraie 24750 trelissac",
        "lat": 45.19161968,
        "id": "6873",
        "lng": 0.76346865,
        "name": "Micromania 6873"
    },
    {
        "address": "Centre commercial GEANT Les grandes Chaumes - RN 10 16430 champniers",
        "lat": 45.71499099,
        "id": "6635",
        "lng": 0.18085817,
        "name": "Micromania 6635"
    },
    {
        "address": "Centre commercial Leclerc 11  Boulevard du 11 Novembre 17300 rochefort",
        "lat": 45.92966316,
        "id": "6743",
        "lng": -0.95906685,
        "name": "Micromania 6743"
    },
    {
        "address": "Centre Commercial CARREFOUR RN137 17690 angoulins",
        "lat": 46.11127017,
        "id": "6630",
        "lng": -1.10444228,
        "name": "Micromania 6630"
    },
    {
        "address": "Centre Commercial Carrefour CENTRE COMMERCIAL CARREFOUR 19100 brive la gaillarde",
        "lat": 45.145701438524,
        "id": "6920",
        "lng": 1.4825412663352,
        "name": "Micromania 6920"
    },
    {
        "address": "Centre Commercial BAB2 6 rue Jean L\u00e9on Laporte 64600 anglet",
        "lat": 43.48576006,
        "id": "6702",
        "lng": -1.50228295,
        "name": "Micromania 6702"
    },
    {
        "address": "Centre commercial GEANT Route de Paris 79000 niort",
        "lat": 46.3408025,
        "id": "6790",
        "lng": -0.37514459,
        "name": "Micromania 6790"
    },
    {
        "address": "Centre commercial CARREFOUR ROUTE DE BAYONNE 64230 lescar",
        "lat": 43.31766506,
        "id": "6596",
        "lng": -0.42463554,
        "name": "Micromania 6596"
    },
    {
        "address": "CCial Auchan Local 18  9 av Gal Leclerc 64000 pau",
        "lat": 43.30550885,
        "id": "6814",
        "lng": -0.33273996,
        "name": "Micromania 6814"
    },
    {
        "address": "Centre commercial Auchan 777 AVENUE JEAN MOULIN 82000 montauban",
        "lat": 44.029150561726,
        "id": "6794",
        "lng": 1.3698957787101,
        "name": "Micromania 6794"
    },
    {
        "address": "Centre commercial CARREFOUR CORGNAC 14  Rue Georges Briquet 87000 limoges",
        "lat": 45.83957136,
        "id": "6600",
        "lng": 1.23347162,
        "name": "Micromania 6600"
    },
    {
        "address": "Centre Commercial AUCHAN POITIERS SUD 250 avenue du 8 mai 45 86000 poitiers",
        "lat": 46.5527095,
        "id": "6822",
        "lng": 0.30552938,
        "name": "Micromania 6822"
    },
    {
        "address": "Centre commercial GEANT FENOUILLET RN20 ZI Saint JORY 31150 fenouillet",
        "lat": 43.68874325,
        "id": "6872",
        "lng": 1.40246243,
        "name": "Micromania 6872"
    },
    {
        "address": "Centre Commercial GEANT Route de Talmont 85100 les sables d olonne",
        "lat": 46.48398599,
        "id": "6801",
        "lng": -1.72570561,
        "name": "Micromania 6801"
    },
    {
        "address": "2 ALLEE EMILE ZOLA ZAC DU GRAND NOBLE 31700 blagnac",
        "lat": 43.64593395,
        "id": "6869",
        "lng": 1.37241678,
        "name": "Micromania 6869"
    },
    {
        "address": "Centre Commercial GEANT BEAULIEU 2 avenue Lafayette 86000 poitiers",
        "lat": 46.57473294,
        "id": "6809",
        "lng": 0.37436552,
        "name": "Micromania 6809"
    },
    {
        "address": "Centre commercial CARREFOUR 36/54 RTE DE BAYONNE 31000 toulouse",
        "lat": 43.61005624,
        "id": "6880",
        "lng": 1.39450983,
        "name": "Micromania 6880"
    },
    {
        "address": "5 rue Saint Rome 31000 toulouse",
        "lat": 43.60192993,
        "id": "6877",
        "lng": 1.44422855,
        "name": "Micromania 6877"
    },
    {
        "address": "Centre commercial CARREFOUR 36/54 RTE DE BAYONNE 31000 toulouse",
        "lat": 43.61005624,
        "id": "6880",
        "lng": 1.39450983,
        "name": "Micromania 6880"
    },
    {
        "address": "Centre commercial OCCITANIA Chemin de Gabarie 31000 toulouse",
        "lat": 43.63638193,
        "id": "6737",
        "lng": 1.48295772,
        "name": "Micromania 6737"
    },
    {
        "address": "Centre Commercial LECLERC ORENS Route de Revel Rocade EST 31650 saint orens de gameville",
        "lat": 43.56625392,
        "id": "6885",
        "lng": 1.51791064,
        "name": "Micromania 6885"
    },
    {
        "address": "2 ALLEE EMILE ZOLA ZAC DU GRAND NOBLE 31700 blagnac",
        "lat": 43.64593395,
        "id": "6869",
        "lng": 1.37241678,
        "name": "Micromania 6869"
    },
    {
        "address": "Centre commercial LABEGE 2 Zac la Grande Borde 31670 labege",
        "lat": 43.55083235,
        "id": "6876",
        "lng": 1.50877733,
        "name": "Micromania 6876"
    },
    {
        "address": "Centre commercial CARREFOUR Bld de l\u00b4Europe - Local 85 31120 portet sur garonne",
        "lat": 43.53370907,
        "id": "6879",
        "lng": 1.40395887,
        "name": "Micromania 6879"
    },
    {
        "address": "Centre commercial LECLERC RN 88  Route d\u00b4Albi 31180 rouffiac tolosan",
        "lat": 43.66950188,
        "id": "6868",
        "lng": 1.51609358,
        "name": "Micromania 6868"
    },
    {
        "address": "Centre commercial GEANT FENOUILLET RN20 ZI Saint JORY 31150 fenouillet",
        "lat": 43.68874325,
        "id": "6872",
        "lng": 1.40246243,
        "name": "Micromania 6872"
    },
    {
        "address": "Centre commercial Leclerc All\u00e9e De Fraixinet 31120 roques",
        "lat": 43.51522318,
        "id": "6802",
        "lng": 1.37357164,
        "name": "Micromania 6802"
    },
    {
        "address": "Centre commercial Auchan 777 AVENUE JEAN MOULIN 82000 montauban",
        "lat": 44.029150561726,
        "id": "6794",
        "lng": 1.3698957787101,
        "name": "Micromania 6794"
    },
    {
        "address": "Centre Commercial AUCHAN ROUTE DE TOULOUSE 81100 castres",
        "lat": 43.58958771,
        "id": "6684",
        "lng": 2.1886041,
        "name": "Micromania 6684"
    },
    {
        "address": "Centre commercial Leclerc \u00b4Les portes d\u00b4Albi\u00b4 ZAC de Fonlabour 81000 albi",
        "lat": 43.91879284,
        "id": "6646",
        "lng": 2.09691912,
        "name": "Micromania 6646"
    },
    {
        "address": "C.C Leclerc Lieut dit L\u00b4Estr\u00e9niol 12850 sainte radegonde",
        "lat": 44.37031667,
        "id": "6918",
        "lng": 2.58917946,
        "name": "Micromania 6918"
    },
    {
        "address": "CCial Auchan Local 18  9 av Gal Leclerc 64000 pau",
        "lat": 43.30550885,
        "id": "6814",
        "lng": -0.33273996,
        "name": "Micromania 6814"
    },
    {
        "address": "Centre commercial AUCHAN 4 av Voie Domitienne 34500 beziers",
        "lat": 43.34804619,
        "id": "6665",
        "lng": 3.24823279,
        "name": "Micromania 6665"
    },
    {
        "address": "Ccial G\u00e9ant Zac de Montimaran 34500 beziers",
        "lat": 43.34183018,
        "id": "6672",
        "lng": 3.26265023,
        "name": "Micromania 6672"
    },
    {
        "address": "Centre commercial LECLERC 2130 Av. du Languedoc 66000 perpignan",
        "lat": 42.74546517,
        "id": "6813",
        "lng": 2.89413633,
        "name": "Micromania 6813"
    },
    {
        "address": "Route d\u00e9partementale 83 66530 claira",
        "lat": 42.76102549,
        "id": "6819",
        "lng": 2.95686084,
        "name": "Micromania 6819"
    },
    {
        "address": "Centre commercial CARREFOUR ROUTE DE BAYONNE 64230 lescar",
        "lat": 43.31766506,
        "id": "6596",
        "lng": -0.42463554,
        "name": "Micromania 6596"
    },
    {
        "address": "Centre commercial PORTE D\u00b4ESPAGNE 66000 perpignan",
        "lat": 42.66037807,
        "id": "6812",
        "lng": 2.88384135,
        "name": "Micromania 6812"
    },
    {
        "address": "Centre Commercial Ch\u00e2teau Roussillon Route du Canet 66000 perpignan",
        "lat": 42.70210462,
        "id": "6747",
        "lng": 2.940171,
        "name": "Micromania 6747"
    },
    {
        "address": "Centre commercial LECLERC Rives de Dordogne 24100 bergerac",
        "lat": 44.83418126,
        "id": "6724",
        "lng": 0.45064739,
        "name": "Micromania 6724"
    },
    {
        "address": "Centre Commercial Carrefour CENTRE COMMERCIAL CARREFOUR 19100 brive la gaillarde",
        "lat": 45.145701438524,
        "id": "6920",
        "lng": 1.4825412663352,
        "name": "Micromania 6920"
    },
    {
        "address": "Centre commercial LECLERC La Feuilleraie 24750 trelissac",
        "lat": 45.19161968,
        "id": "6873",
        "lng": 0.76346865,
        "name": "Micromania 6873"
    },
    {
        "address": "Centre commercial Auchan Avenue Louis Suder 24430 marsac sur l isle",
        "lat": 45.19282295,
        "id": "6614",
        "lng": 0.65876414,
        "name": "Micromania 6614"
    },
    {
        "address": "Centre Commercial Trifontaine Route de Ganges 34980 saint clement de riviere",
        "lat": 43.6483398,
        "id": "6674",
        "lng": 3.84267698,
        "name": "Micromania 6674"
    },
    {
        "address": "Centre commercial Carrefour Route S\u00e8te 34430 saint jean de vedas",
        "lat": 43.56986179,
        "id": "6854",
        "lng": 3.8502155,
        "name": "Micromania 6854"
    },
    {
        "address": "Centre commercial POLYGONE 1  Rue des Perthuisanes Local 305 34000 montpellier",
        "lat": 43.60871711,
        "id": "6768",
        "lng": 3.88563713,
        "name": "Micromania 6768"
    },
    {
        "address": "Centre commercial CARREFOUR Grand Sud Route de Carnon 34970 lattes",
        "lat": 43.5847089,
        "id": "6764",
        "lng": 3.92199849,
        "name": "Micromania 6764"
    }
    ]);  
  } 