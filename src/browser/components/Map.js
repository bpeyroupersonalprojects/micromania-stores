import React, { Component, PropTypes } from 'react';
import ReactDOM from 'react-dom';
import {Gmaps, Marker, InfoWindow, Circle} from 'react-gmaps';
import { connect } from 'react-redux';
import { compose, isEmpty, prop, reverse, sortBy, values } from 'ramda';
import { loadDatas, selectStore, unselectStore } from '../../common/micromaniaStores/actions';
import { Heading, Text, Button } from '../../common/components';
import { CSVLink } from 'react-csv';

const coords = [
  {
    lat: 48.856614,
    lng: 2.3522219
  },
  {
    lat: 48.85,
    lng: 2.35
  }
];

const params = {v: '3', key: 'AIzaSyA0wOfRnczFBs10b9zpCa8L6yqoJ1S6J_c'};

class Map extends React.Component {

  
  componentWillMount() {
    console.log(this.props.stores);
    this.props.loadstores(center={lat: 48.856614,lng: 2.3522219});
    console.log(this.props.stores);
  }
  

  onMapCreated(map) {
    map.setOptions({
      disableDefaultUI: false
    });
  }

  onDragEnd(e) {
    console.log('onDragEnd', e);
  }

  onCloseClick() {
    console.log('onCloseClick');
  }

  onClick(idx, e) {
    this.props.selectStore(idx);
  }

  onRightClick(idx, e) {
    this.props.unselectStore(idx);
  }

  render() {
    const {stores} = this.props;
    return (
      <div>
        <Gmaps
          width={'800px'}
          height={'600px'}
          lat={coords[0].lat}
          lng={coords[0].lng}
          zoom={12}
          loadingMessage={'Loading Map ...'}
          params={params}
          onMapCreated={this.onMapCreated}>
          {stores.map(store => {
            return (
              <Marker
                key={store.id}
                lat={store.lat}
                lng={store.lng}
                draggable={false}
                onRightClick={this.onRightClick.bind(this, store.id)}
                onClick={this.onClick.bind(this, store.id)} />);
          })}
          <InfoWindow
            lat={coords[0].lat}
            lng={coords[0].lng}
            content={'Click on Stores to select them, right click to deselect'}
            onCloseClick={this.onCloseClick} />
        </Gmaps>
        <Heading/>
        <Heading size={1}>
          Selected Store
        </Heading>
          <Text>
            { stores &&
                <ul>
                    {this.props.stores.map(store => {
                      if (store.selected){
                        return ( <li key={store.id}>nom : {store.name} - adresse : {store.address}</li>);
                      }
                    })}
                </ul>
            }
          </Text>
          <CSVLink data={this.props.stores.filter(store => store.selected)}
            filename={"stores.csv"}
            className="btn btn-primary"
            target="_blank">
            Download as CSV
          </CSVLink>
      </div>
    );
  }

};

Map.propTypes = {
  stores: PropTypes.array,
  loadstores: PropTypes.func.isRequired,
  selectStore: PropTypes.func.isRequired,
  unselectStore: PropTypes.func.isRequired,
}

const mapDispatchToProps = {
    loadstores : (center) => loadDatas(center),
    selectStore : (idx) => selectStore(idx),
    unselectStore : (idx) => unselectStore(idx)
}

const mapStateToProps = (state) => {
  return{
      stores: state.micromaniaStores
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Map);