// @flow
import * as themes from '../themes';
import React from 'react';
import {
  Box,
  Button,
  Heading,
  Image,
  PageHeader,
  Paragraph,
  SwitchTheme,
  Text,
  ToggleBaseline,
} from '../../common/components';
import { Link, Title } from '../components';
import Map from '../components/Map'

const HomePage = () => (
  <Box>
    <Title message="Micromania Stores" />
    <PageHeader
      heading="Micromania Stores"
      description="Webapp to see Micromania stores and export them as csv."
    />
    <Heading size={1}>
      Map
    </Heading>
    
    <Paragraph>
      Click on stores to select them.
    </Paragraph>

    <Map/>
  </Box>
);

export default HomePage;

